import React, {useState} from 'react'
import {Link} from 'react-router-dom'
import {Row, Col, Button} from 'react-bootstrap';

export default function Banner() {
	return (

		<Row>

			<Col className = "p-5">

				<h1>Error 404</h1>
				<h4>The page you are trying to access is not available.</h4>


				<p>Go back to {' '}
					<Link as={Link} to="/">Homepage</Link>
				</p>

			</Col>

		</Row>

	)
};
